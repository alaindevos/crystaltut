# Create a mutable list of integers
list = [1, 2, 3]
# Add an element to the end of the list
list << 4
# Insert an element at the beginning of the list
list.insert(0, 0)
# Remove the last element from the list
list.pop
# Remove the first element from the list
list.shift
# Print the list
puts list
# Get the length of the list
puts list.size
# Check if the list is empty
puts list.empty?
