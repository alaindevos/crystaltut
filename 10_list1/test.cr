struct Node
  property data : Int32
  property next : Node?

  def initialize(@data : Int32, @next : Node? = nil)
  end
end

struct LinkedList
  property head : Node?

  def initialize(@head : Node? = nil)
  end

  # Add a node to the beginning of the list
  def push(data : Int32)
    new_node = Node.new(data, @head)
    @head = new_node
  end

  # Remove the first node from the list
  def pop
    if @head.nil?
      return nil
    end

    removed_node = @head
    @head = @head.next
    removed_node.next = nil
    removed_node.data
  end

  # Delete a node with a given data value
  def delete(data : Int32)
    if @head.nil?
      return
    end

    if @head.data == data
      @head = @head.next
      return
    end

    current_node = @head
    while current_node.next != nil
      if current_node.next.data == data
        current_node.next = current_node.next.next
        return
      end
      current_node = current_node.next
    end
  end

  # Print the list
  def print
    current_node = @head
    while current_node != nil
      print current_node.data, " "
      current_node = current_node.next
    end
    puts
  end
end

# Example usage:
list = LinkedList.new
list.push(10)
list.push(20)
list.push(30)
list.print # Output: 30 20 10

list.delete(20)
list.print # Output: 30 10

list.pop
list.print # Output: 30

